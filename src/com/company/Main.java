package com.company;

import java.io.*;
import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);

    static class Weapon {
        static int LastId = 0;

        int Id;
        String Name;
        int Price;
        int IssueYear;
        String TypeOfWeapon;

        Weapon(int Id, String Name, int Price, int IssueYear, String TypeOfWeapon)
        {
            this.Id = Id;
            this.Name = Name;
            this.Price = Price;
            this.IssueYear = IssueYear;
            this.TypeOfWeapon = TypeOfWeapon;
        }
    }
    //----------------System Functions------------------------//
    static Weapon[] ResizeArray(Weapon[] oldWeapons, int newLength)
    {
        Weapon[] newWeapons = new Weapon[newLength];

        int minLength = newLength < oldWeapons.length ? newLength : oldWeapons.length;

        for (int i=0; i<minLength; i++)
        {
            newWeapons[i] = oldWeapons[i];
        }

        return newWeapons;
    }

    static void PrintMenu()
    {
        System.out.println("1. Add weapon");
        System.out.println("2. Delete weapon by id");
        System.out.println("3. Clear array");
        System.out.println("4. Update weapon by id");
        System.out.println("5. Insert weapon into position");
        System.out.println("6. Print weapon by id");
        System.out.println("7. Print weapons between min and max prices");
        System.out.println("8. Print weapons between min and max issues of year");
        System.out.println("9. Sort weapons by id");
        System.out.println("10. Sort weapons by price");
        System.out.println("11. Sort weapons by issue of year");
        System.out.println("12. Print cars to txt file");
        System.out.println("13. Save cars to data file");
        System.out.println("14. Load cars from data file");
        System.out.println("0. Exit");
    }

    static int InputMenuPoint()
    {
        System.out.print("Input Menu Point: ");
        return scanner.nextInt();
    }

    static void Print50Enters()
    {
        for (int i=0;i<50;i++)
        {
            System.out.println();
        }
    }
    //----------------System Functions------------------------//

    //----------------CRUD Functions------------------------//
    static Weapon[] AddToEnd(Weapon[] weapons, Weapon weapon)
    {
        if(weapons == null)
        {
            weapons = new Weapon[1];
        }
        else
        {
            weapons = ResizeArray(weapons, weapons.length+1);
        }
        weapons[weapons.length-1] = weapon;

        return weapons;
    }

    static Weapon[] DeleteById(Weapon[] weapons, int id)
    {
        int indexDel = GetWeaponIndexById(weapons, id);

        if(indexDel==-1)
        {
            System.out.println("Delete impossible. Element not found");
            return weapons;
        }

        Weapon[] newWeapons = new Weapon[weapons.length-1];
        int newI = 0;

        for (int i=0;i<weapons.length;i++)
        {
            if(i!=indexDel)
            {
                newWeapons[newI] = weapons[i];
                newI++;
            }
        }

        return newWeapons;
    }

    static void UpdateWeaponById(Weapon[] weapons, int id, Weapon weapon)
    {
        int indexUpd = GetWeaponIndexById(weapons, id);

        if(indexUpd==-1)
        {
            System.out.println("Update impossible. Element not found");
            return;
        }

        weapon.Id = id;
        weapons[indexUpd] = weapon;
    }

    static Weapon[] InsertWeaponIntoPosition(Weapon[] weapons, int position, Weapon weapon)
    {
        if(position<1 || position>weapons.length)
        {
            System.out.println("Insert impossible. Position not found");
            return weapons;
        }

        int indexIns = position - 1;

        Weapon[] newWeapons = new Weapon[weapons.length+1];
        int newI=0;

        for (int i=0;i<weapons.length;i++)
        {
            if(newI!=indexIns)
            {
                newWeapons[newI]=weapons[i];
                newI++;
            }
            else
            {
                newWeapons[newI] = weapon;
                newI++;
                i--;
            }
        }

        return newWeapons;
    }
    //----------------CRUD Functions------------------------//

    //----------------Ext Functions------------------------//
    static void SortById(Weapon[] weapons, boolean ASC)
    {
        boolean sort;
        Weapon temp;
        int offset=0;

        do
        {
            sort=true;

            for (int i=0;i<weapons.length-1-offset;i++)
            {
                boolean condition;
                if(ASC)
                {
                    condition = weapons[i+1].Id<weapons[i].Id;
                }
                else
                {
                    condition = weapons[i+1].Id>weapons[i].Id;
                }
                if(condition)
                {
                    sort=false;
                    temp=weapons[i];
                    weapons[i]=weapons[i+1];
                    weapons[i+1]=temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static void SortByPrice(Weapon[] weapons, boolean ASC)
    {
        boolean sort;
        Weapon temp;
        int offset=0;

        do
        {
            sort=true;

            for (int i=0;i<weapons.length-1-offset;i++)
            {
                boolean condition;
                if(ASC)
                {
                    condition = weapons[i+1].Price<weapons[i].Price;
                }
                else
                {
                    condition = weapons[i+1].Price>weapons[i].Price;
                }
                if(condition)
                {
                    sort=false;
                    temp=weapons[i];
                    weapons[i]=weapons[i+1];
                    weapons[i+1]=temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static void SortByIssueYear(Weapon[] weapons, boolean ASC)
    {
        boolean sort;
        Weapon temp;
        int offset=0;

        do
        {
            sort=true;

            for (int i=0;i<weapons.length-1-offset;i++)
            {
                boolean condition;
                if(ASC)
                {
                    condition = weapons[i+1].IssueYear<weapons[i].IssueYear;
                }
                else
                {
                    condition = weapons[i+1].IssueYear>weapons[i].IssueYear;
                }
                if(condition)
                {
                    sort=false;
                    temp=weapons[i];
                    weapons[i]=weapons[i+1];
                    weapons[i+1]=temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static int GetMinPrice(Weapon[] weapons)
    {
        int minPrice=weapons[0].Price;

        for (int i=0;i<weapons.length;i++)
        {
            if (weapons[i].Price<minPrice)
            {
                minPrice=weapons[i].Price;
            }
        }

        return  minPrice;
    }

    static int GetMaxPrice(Weapon[] weapons)
    {
        int maxPrice=weapons[0].Price;

        for (int i=0;i<weapons.length;i++)
        {
            if (weapons[i].Price>maxPrice)
            {
                maxPrice=weapons[i].Price;
            }
        }

        return  maxPrice;
    }

    static Weapon[] GetWeaponsBetweenMaxPrice(Weapon[] weapons, int minPrice, int maxPrice)
    {
        Weapon[] foundedWeapons = null;
        for (int i=0;i<weapons.length;i++)
        {
            if(weapons[i].Price>=minPrice && weapons[i].Price<=maxPrice)
            {
                foundedWeapons = AddToEnd(foundedWeapons, weapons[i]);
            }
        }

        return foundedWeapons;
    }

    static int GetMinIssueYear(Weapon[] weapons)
    {
        int minIssueYear=weapons[0].IssueYear;

        for (int i=0;i<weapons.length;i++)
        {
            if (weapons[i].IssueYear<minIssueYear)
            {
                minIssueYear=weapons[i].IssueYear;
            }
        }

        return  minIssueYear;
    }

    static int GetMaxIssueYear(Weapon[] weapons)
    {
        int maxIssueYear=weapons[0].IssueYear;

        for (int i=0;i<weapons.length;i++)
        {
            if (weapons[i].IssueYear>maxIssueYear)
            {
                maxIssueYear=weapons[i].IssueYear;
            }
        }

        return  maxIssueYear;
    }

    static Weapon[] GetWeaponsBetweenMaxIssueYear(Weapon[] weapons, int minIssueYear, int maxIssueYear)
    {
        Weapon[] foundedWeapons = null;
        for (int i=0;i<weapons.length;i++)
        {
            if(weapons[i].IssueYear>=minIssueYear && weapons[i].IssueYear<=maxIssueYear)
            {
                foundedWeapons = AddToEnd(foundedWeapons, weapons[i]);
            }
        }

        return foundedWeapons;
    }

    static void PrintSingle(Weapon weapon)
    {
        System.out.printf("%-4d%-12s%-10d%-10d%-10s\n", weapon.Id, weapon.Name, weapon.Price, weapon.IssueYear, weapon.TypeOfWeapon);
    }

    static void PrintAll(Weapon[] weapons)
    {
        System.out.printf("%-4s%-12s%-10s%-10s%-10s\n","Id","Name","Price","IssueYear","TypeOfWeapon");

        if(weapons==null)
        {
            System.out.println("Array is Empty");
        }
        else if(weapons.length==0)
        {
            System.out.println("Array is Empty");
        }
        else
        {
            for (int i = 0; i < weapons.length; i++) {
                PrintSingle(weapons[i]);
            }
        }
        System.out.println("===============================");
    }

    static Weapon GetWeaponById(Weapon[] weapons, int id)
    {
        int indexPrt = GetWeaponIndexById(weapons, id);

        if(indexPrt==-1)
        {
            //System.out.println("Print impossible. Element not found");
            return null;
        }

        return weapons[indexPrt];
    }

    static Weapon CreateWeapon(boolean newId)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input Name: ");
        String name = scanner.next();

        System.out.print("Input Price: ");
        int price = scanner.nextInt();

        System.out.print("Input Issue Year: ");
        int issueYear = scanner.nextInt();

        System.out.print("Input Type of Weapon: ");
        String typeOfWeapon = String.valueOf(scanner.next());

        Weapon weapon = new Weapon(0,name, price, issueYear, typeOfWeapon);

        if (newId)
        {
            Weapon.LastId++;
            weapon.Id = Weapon.LastId;
        }

        return weapon;
    }

    static int GetWeaponIndexById(Weapon[] weapons, int id)
    {
        for (int i=0;i<weapons.length;i++)
        {
            if(weapons[i].Id == id)
            {
                return i;
            }
        }
        return -1;
    }
    //----------------Ext Functions------------------------//

    //----------------File Functions------------------------//
    static void PrintAllToFile(Weapon[] weapons, String fileName) throws FileNotFoundException {
        PrintStream stream = new PrintStream(fileName);

        stream.printf("%-4s%-12s%-10s%-10s%-10s","Id","Name","Price","IssueYear","TypeOfWeapon");
        stream.println();

        if(weapons==null)
        {
            stream.println("Array is Empty");
        }
        else if(weapons.length==0)
        {
            stream.println("Array is Empty");
        }
        else
        {
            for (int i = 0; i < weapons.length; i++) {
                stream.printf("%-4d%-12s%-10d%-10d%-10s", weapons[i].Id, weapons[i].Name, weapons[i].Price, weapons[i].IssueYear, weapons[i].TypeOfWeapon);
                stream.println();
            }
        }

        stream.close();
    }

    static void SaveWeaponsToFile(Weapon[] weapons, String fileName) throws FileNotFoundException {
        PrintStream stream = new PrintStream(fileName);

        stream.println(weapons.length);
        stream.println(Weapon.LastId);

        for (int i=0;i<weapons.length;i++)
        {
            stream.println(weapons[i].Id);
            stream.println(weapons[i].Name);
            stream.println(weapons[i].Price);
            stream.println(weapons[i].IssueYear);
            stream.println(weapons[i].TypeOfWeapon);
        }

        stream.close();
    }

    static Weapon[] LoadWeaponsFromFile(String fileName) throws IOException {
        Weapon[] weapons;
        int countWeapons;

        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        countWeapons = Integer.parseInt(reader.readLine());
        Weapon.LastId = Integer.parseInt(reader.readLine());

        weapons = new Weapon[countWeapons];

        for (int i=0;i<weapons.length;i++)
        {
            int id = Integer.parseInt(reader.readLine());
            String name = reader.readLine();
            int price = Integer.parseInt(reader.readLine());
            int issueYear = Integer.parseInt(reader.readLine());
            String typeOfWeapon = reader.readLine();

            weapons[i] = new Weapon(id, name,price,issueYear,typeOfWeapon);
        }

        reader.close();

        return weapons;
    }
    //----------------File Functions------------------------//

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        Weapon[] weapons = null;
        int menuPoint;

        Weapon weapon;
        int id;
        boolean ASC;
        String fileName;

        do {
            Print50Enters();
            PrintAll(weapons);
            PrintMenu();

            menuPoint = InputMenuPoint();

            switch (menuPoint)
            {
                case 14:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    weapons = LoadWeaponsFromFile(fileName);
                    break;
                case 13:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    SaveWeaponsToFile(weapons,fileName);
                    break;
                case 12:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    PrintAllToFile(weapons, fileName);
                    break;
                case 11:
                    System.out.print("in ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    SortByIssueYear(weapons,ASC);
                    break;
                case 10:
                    System.out.print("in ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    SortByPrice(weapons,ASC);
                    break;
                case 9:
                    System.out.print("in ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    SortById(weapons,ASC);
                    break;
                case 8:
                    System.out.println("Input min and max issues of year between: "+GetMinIssueYear(weapons)+" and "+GetMaxIssueYear(weapons));

                    System.out.print("min: ");
                    int minIssueYear = scanner.nextInt();

                    System.out.print("max: ");
                    int maxIssueYear = scanner.nextInt();

                    PrintAll(GetWeaponsBetweenMaxIssueYear(weapons,minIssueYear,maxIssueYear));
                    break;
                case 7:
                    System.out.println("Input min and max prices between: "+GetMinPrice(weapons)+" and "+GetMaxPrice(weapons));

                    System.out.print("min: ");
                    int minPrice = scanner.nextInt();

                    System.out.print("max: ");
                    int maxPrice = scanner.nextInt();

                    PrintAll(GetWeaponsBetweenMaxPrice(weapons,minPrice,maxPrice));
                    break;
                case 6:
                    System.out.print("Input id: ");
                    id=scanner.nextInt();
                    weapon = GetWeaponById(weapons,id);
                    if(weapon==null)
                    {
                        System.out.println("Print impossible. Element not found");
                    }
                    else
                    {
                        PrintSingle(weapon);
                    }
                    break;
                case 5:
                    System.out.print("Input position: ");
                    int position = scanner.nextInt();

                    weapon = CreateWeapon(true);

                    weapons=InsertWeaponIntoPosition(weapons,position,weapon);
                    break;

                case 4:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();

                    weapon = CreateWeapon(false);

                    UpdateWeaponById(weapons,id,weapon);
                    break;

                case 3:
                    weapons = null;
                    break;

                case 2:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();
                    weapons = DeleteById(weapons,id);
                    break;

                case 1:
                    weapon = CreateWeapon(true);
                    weapons = AddToEnd(weapons, weapon);
                    break;

                case 0:
                    System.out.println("Program will be finished");
                    break;

                default:
                    System.out.println("Unknown menu point");
                    break;
            }

            System.in.read();
        }
        while (menuPoint!=0);
    }
}